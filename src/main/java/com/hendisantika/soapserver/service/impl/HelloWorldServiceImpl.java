package com.hendisantika.soapserver.service.impl;

import com.hendisantika.soapserver.model.ApplicationCredentials;
import com.hendisantika.soapserver.service.HelloWorldService;

import javax.jws.WebService;

/**
 * Created by IntelliJ IDEA.
 * Project : soap-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/03/18
 * Time: 18.30
 * To change this template use File | Settings | File Templates.
 */
@WebService(endpointInterface = "com.hendisantika.soapserver.service.HelloWorldService")
public class HelloWorldServiceImpl implements HelloWorldService {

    @Override
    public String helloWorld(final String name,
                             final ApplicationCredentials credential) {
        return "Hello World from " + name;
    }
}