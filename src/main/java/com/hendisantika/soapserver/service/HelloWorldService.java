package com.hendisantika.soapserver.service;

import com.hendisantika.soapserver.model.ApplicationCredentials;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by IntelliJ IDEA.
 * Project : soap-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/03/18
 * Time: 18.27
 * To change this template use File | Settings | File Templates.
 */
@WebService
public interface HelloWorldService {

    @WebMethod(operationName = "helloWorld", action = "https://aggarwalarpit.wordpress.com/hello-world/helloWorld")
    String helloWorld(final String name,
                      @WebParam(header = true) final ApplicationCredentials credential);

}